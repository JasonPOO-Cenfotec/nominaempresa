package cr.ac.ucenfotec.bl;

public class Salario {

    private double monto;
    private double cargasSociales;
    private double montoDiario;
    private int cantidadDiasTrabajados;

    public Salario() {
    }

    public Salario(double monto, double cargasSociales, double montoDiario, int cantidadDiasTrabajados) {
        this.monto = monto;
        this.cargasSociales = cargasSociales;
        this.montoDiario = montoDiario;
        this.cantidadDiasTrabajados = cantidadDiasTrabajados;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double getCargasSociales() {
        return cargasSociales;
    }

    public void setCargasSociales(double cargasSociales) {
        this.cargasSociales = cargasSociales;
    }

    public double getMontoDiario() {
        return montoDiario;
    }

    public void setMontoDiario(double montoDiario) {
        this.montoDiario = montoDiario;
    }

    public int getCantidadDiasTrabajados() {
        return cantidadDiasTrabajados;
    }

    public void setCantidadDiasTrabajados(int cantidadDiasTrabajados) {
        this.cantidadDiasTrabajados = cantidadDiasTrabajados;
    }

    public void calcularSalario(int diaDisponibles, int diasDisfrutados){
        // Ejemplo de relación de dependencia
        Vacaciones vaca = new Vacaciones(diaDisponibles,diasDisfrutados);
        this.monto = (vaca.getDiasRestantes() * this.montoDiario) -this.cargasSociales;
    }

    @Override
    public String toString() {
        return "Salario{" +
                "monto=" + monto +
                ", cargasSociales=" + cargasSociales +
                ", montoDiario=" + montoDiario +
                ", cantidadDiasTrabajados=" + cantidadDiasTrabajados +
                '}';
    }
}
