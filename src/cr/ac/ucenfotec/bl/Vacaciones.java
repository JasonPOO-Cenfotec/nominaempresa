package cr.ac.ucenfotec.bl;

public class Vacaciones {

    private int DiasDisponibles;
    private int DiasDisfrutados;

    public Vacaciones() {
    }

    public Vacaciones(int diasDisponibles, int diasDisfrutados) {
        DiasDisponibles = diasDisponibles;
        DiasDisfrutados = diasDisfrutados;
    }

    public int getDiasDisponibles() {
        return DiasDisponibles;
    }

    public void setDiasDisponibles(int diasDisponibles) {
        DiasDisponibles = diasDisponibles;
    }

    public int getDiasDisfrutados() {
        return DiasDisfrutados;
    }

    public void setDiasDisfrutados(int diasDisfrutados) {
        DiasDisfrutados = diasDisfrutados;
    }

    public int getDiasRestantes(){
        //el código que sea necesario para realizar calculo
        return this.DiasDisponibles - this.DiasDisfrutados;

    }

    @Override
    public String toString() {
        return "Vacaciones{" +
                "DiasDisponibles=" + DiasDisponibles +
                ", DiasDisfrutados=" + DiasDisfrutados +
                '}';
    }
}
